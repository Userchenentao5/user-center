create table user
(
    id            bigint auto_increment comment 'id'
        primary key,
    username      varchar(256)      null comment '用户名',
    user_account  varchar(256)      not null comment '账号',
    avatar_url    varchar(1024)     null comment '用户头像',
    gender        tinyint           null comment '性别',
    user_password varchar(512)      not null comment '密码',
    phone         varchar(128)      null comment '电话',
    email         varchar(512)      null comment '邮箱',
    user_status   int     default 0 not null comment '状态 0-正常',
    user_role     int     default 0 not null comment '角色，0-普通用户 1-管理员 2-vip',
    create_time   datetime          null comment '创建时间',
    update_time   datetime          null comment '修改时间',
    deleted       tinyint default 0 not null comment '逻辑删除',
    planet_code   varchar(512)      null comment '星球编号'
)
    comment '用户表';
