package com.cet.usercenter;

import com.cet.usercenter.mapper.UserMapper;
import com.cet.usercenter.model.domain.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
class UserCenterApplicationTests {

    @Resource
    private UserMapper userMapper;

    @Test
    void contextLoads() {
    }

    @Test
    void testSelect() {
        List<User> userList = userMapper.selectList(null);
        Assertions.assertEquals(5, userList.size());
        userList.forEach(System.out::println);

    }

    @Test
    void testMd5Encoding() {
        System.out.println(DigestUtils.md5DigestAsHex("adcdefg".getBytes()));
    }

}
