package com.cet.usercenter.service;

import com.cet.usercenter.model.domain.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

/**
 * 用户服务测试
 * @author cet
 */
@SpringBootTest
class UserServiceTest {

    @Resource
    private UserService userService;

    @Test
    void testInsert() {
        User user = new User();
        user.setUsername("张三");
        user.setUserAccount("zhangsan");
        user.setAvatarUrl("https://image.baidu.com/search/detail?ct=503316480&z=0&ipn=d&word=mybatisplus&step_word=&hs=0&pn=8&spn=0&di=7214885350303334401&pi=0&rn=1&tn=baiduimagedetail&is=0%2C0&istype=0&ie=utf-8&oe=utf-8&in=&cl=2&lm=-1&st=undefined&cs=3827164769%2C3538685520&os=3083318572%2C1596644943&simid=3827164769%2C3538685520&adpicid=0&lpn=0&ln=1268&fr=&fmq=1684379936691_R&fm=&ic=undefined&s=undefined&hd=undefined&latest=undefined&copyright=undefined&se=&sme=&tab=0&width=undefined&height=undefined&face=undefined&ist=&jit=&cg=&bdtype=0&oriquery=&objurl=https%3A%2F%2Fpic1.zhimg.com%2Fv2-874e905841e44527a01da655c926cd83_1440w.jpg%3Fsource%3D172ae18b&fromurl=ippr_z2C%24qAzdH3FAzdH3Fzi7wgswg_z%26e3Bziti7_z%26e3Bv54AzdH3FrAzdH3Fdm99dln8b&gsm=1e&rpstart=0&rpnum=0&islist=&querylist=&nojc=undefined&dyTabStr=MCwxLDYsNSwzLDQsMiw3LDgsOQ%3D%3D");
        user.setGender(0);
        user.setUserPassword("xxx");
        user.setPhone("123");
        user.setEmail("456");
        boolean success = userService.save(user);
        Assertions.assertTrue(success);

    }

    @Test
    void testRegister() {
        long id = userService.userRegister("cet03", "sssssssss", "sssssssss", "9527");
        Assertions.assertNotEquals(-1, id);
    }

    @Test
    void testLogicDel() {
        userService.removeById(2L);
    }

    @Test
    void testSelectAll() {
        userService.list().forEach(System.out::println);
    }

}
