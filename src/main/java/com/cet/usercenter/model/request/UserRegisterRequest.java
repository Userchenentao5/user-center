package com.cet.usercenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: user-center
 * @description: 用户注册请求体
 * @author: 陈恩涛
 * @create: 2023-05-18 22:17
 **/

@Data
public class UserRegisterRequest implements Serializable {

    private static final long serialVersionUID = -1906064309543440471L;

    private String userAccount;

    private String userPassword;

    private String checkPassword;

    private String planetCode;
}
