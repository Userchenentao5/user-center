package com.cet.usercenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: user-center
 * @description: 用户登录请求体
 * @author: 陈恩涛
 * @create: 2023-05-18 22:27
 **/

@Data
public class UserLoginRequest implements Serializable {
    private static final long serialVersionUID = 5172043358440847976L;

    private String userAccount;

    private String userPassword;
}
