package com.cet.usercenter.constant;

/**
 * @program: user-center
 * @description: 用户常量
 * @author: 陈恩涛
 * @create: 2023-05-18 23:59
 **/
public interface UserConstant {

    /**
     * 用户登录状态
     */
    String USER_LOGIN_STATE = "userLoginState";

    // ---------权限-----------

    /**
     * 普通用户权限
     */
    int DEFAULT_ROLE = 0;

    /**
     * 管理员权限
     */
    int ADMIN_ROLE = 1;


}
