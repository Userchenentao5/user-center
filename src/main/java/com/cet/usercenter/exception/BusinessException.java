package com.cet.usercenter.exception;

import com.cet.usercenter.common.ErrorCode;

/**
 * @program: user-center
 * @description: 自定义异常类
 * @author: 陈恩涛
 * @create: 2023-05-19 22:48
 **/
public class BusinessException extends RuntimeException{
    private static final long serialVersionUID = 297790717142076994L;

    private final int code;

    private final String description;

    public BusinessException(String message, int code, String description) {
        super(message);
        this.code = code;
        this.description = description;
    }

    public BusinessException(ErrorCode errorCode) {
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
        this.description = errorCode.getDescription();
    }

    public BusinessException(ErrorCode errorCode, String description) {
        super(errorCode.getMessage());
        this.code = errorCode.getCode();
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
