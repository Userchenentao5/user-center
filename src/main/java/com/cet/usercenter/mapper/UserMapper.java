package com.cet.usercenter.mapper;

import com.cet.usercenter.model.domain.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 30952
* @description 针对表【user(用户表)】的数据库操作Mapper
* @createDate 2023-05-18 11:07:50
* @Entity com.cet.usercenter.model.domain.User
*/
public interface UserMapper extends BaseMapper<User> {

}




