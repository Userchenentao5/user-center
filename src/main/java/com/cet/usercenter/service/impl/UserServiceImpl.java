package com.cet.usercenter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cet.usercenter.common.ErrorCode;
import com.cet.usercenter.exception.BusinessException;
import com.cet.usercenter.mapper.UserMapper;
import com.cet.usercenter.model.domain.User;
import com.cet.usercenter.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.cet.usercenter.constant.UserConstant.USER_LOGIN_STATE;

/**
* @author 30952
* @description 针对表【user(用户表)】的数据库操作Service实现
* @createDate 2023-05-18 11:07:50
*/

@Slf4j
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService{

    @Value("${regex.valid.account}")
    private String validAccountString;

    @Value("${yupi.salt}")
    private String salt;


    @Override
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public long userRegister(String userAccount, String userPassword, String checkPassword, String planetCode) {

        // 1.非空校验
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword, planetCode)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "参数为空");
        }
        // 2.账户不小于4位
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账户长度小于4位");
        }
        // 3.密码不小于8位
        if (userPassword.length() < 8 || checkPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码长度小于8位");
        }

        // 星球长度不能超过5
        if (planetCode.length() > 5) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "星球编号大于5位");
        }

        // 账户不包含特殊字符
        Pattern p = Pattern.compile(validAccountString);
        Matcher matcher = p.matcher(userAccount);
        if (matcher.matches()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账户包含特殊字符");
        }

        // 密码和校验密码相同
        if (!userPassword.equals(checkPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "两次输入密码不同");
        }

        // 对密码进行加密
        String encryptPassword = DigestUtils.md5DigestAsHex((salt + userPassword).getBytes());

        User user = new User();

        user
            .setUserAccount(userAccount)
            .setUserPassword(encryptPassword)
            .setPlanetCode(planetCode);

        //账户不能重复
        Long userAccountCount = this.lambdaQuery()
            .eq(User::getUserAccount, userAccount)
            .count();

        Long planetCodeCount = this.lambdaQuery()
            .eq(User::getPlanetCode, planetCode)
            .count();

        // it is possible to attribute to deadlock
//        synchronized (userAccount.intern()) {
//            synchronized (planetCode.intern()) {

        // this pressure of registration is fine
        synchronized (this) {
            if (userAccountCount > 0) {
                log.info("{}，该账户已存在！", userAccount);
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "账户已经存在");
            }

            if (planetCodeCount > 0) {
                log.info("{}，该星球编号已存在！", planetCode);
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "星球编号已经存在");
            }
            // 向数据库插入数据，注意事务失效
            if (!insert(user)) {
                return -1;
            }
        }

        return user.getId();
    }

    /**
     * 插入新用户
     * @param user 用户信息
     * @return 是否插入成功
     */
    public boolean insert(User user) {
        return this.save(user);
    }

    @Override
    public User doLogin(String userAccount, String userPassword, HttpServletRequest request) {
        // 1.非空校验
        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "参数为空");
        }
        // 2.账户不小于4位
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账户长度小于4位");
        }
        // 3.密码不小于8位
        if (userPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码长度小于8位");
        }

        // 账户不包含特殊字符
        Pattern p = Pattern.compile(validAccountString);
        Matcher matcher = p.matcher(userAccount);
        if (matcher.matches()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账户包含特殊字符");
        }

        String encryptPassword = DigestUtils.md5DigestAsHex((salt + userPassword).getBytes());

        // 查询用户是否存在
        User user = this.lambdaQuery()
            .eq(User::getUserAccount, userAccount)
            .eq(User::getUserPassword, encryptPassword)
            .one();

        if (user == null) {
            log.info("user login failed, user account cannot match password!");
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账密不匹配");
        }

        // 用户数据脱敏
        User safetyUser = getSafetyUser(user);

        // 记录用户的登录态
        HttpSession session = request.getSession();
        session.setAttribute(USER_LOGIN_STATE, safetyUser);

        return safetyUser;
    }


    @Override
    public User getSafetyUser(User user) {
        if (user == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "参数为空");
        }
        User safetyUser = new User();
        safetyUser.setId(user.getId())
            .setUsername(user.getUsername())
            .setUserAccount(user.getUserAccount())
            .setAvatarUrl(user.getAvatarUrl())
            .setGender(user.getGender())
            .setPhone(user.getPhone())
            .setEmail(user.getEmail())
            .setUserStatus(user.getUserStatus())
            .setUserRole(user.getUserRole())
            .setPlanetCode(user.getPlanetCode())
            .setCreateTime(user.getCreateTime());
        return safetyUser;
    }

    @Override
    public int userLogout(HttpServletRequest request) {
        request.getSession().removeAttribute(USER_LOGIN_STATE);
        return 1;
    }
}




