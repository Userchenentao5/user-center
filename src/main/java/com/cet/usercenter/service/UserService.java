package com.cet.usercenter.service;

import com.cet.usercenter.model.domain.User;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
* @author 30952
* @description 针对表【user(用户表)】的数据库操作Service
* @createDate 2023-05-18 11:07:50
*/
public interface UserService extends IService<User> {

    /**
     * 用户注册
     *
     * @param userAccount   账号
     * @param userPassword  密码
     * @param checkPassword 二次校验密码
     * @param planetCode 星球编号
     * @return 新用户id
     */
    long userRegister(String userAccount, String userPassword, String checkPassword, String planetCode);

    /**
     * 用户登录
     * @param userAccount 账号
     * @param userPassword 密码
     * @param request
     * @return 用户脱敏后的信息
     */
    User doLogin(String userAccount, String userPassword, HttpServletRequest request);

    /**
     * 用户数据脱敏
     * @param user 原始用户数据
     * @return 脱敏后的用户数据
     */
    User getSafetyUser(User user);

    /**
     * 用户登出
     *
     * @param request httpRequest
     * @return 1 登出成功
     */
    int userLogout(HttpServletRequest request);
}
