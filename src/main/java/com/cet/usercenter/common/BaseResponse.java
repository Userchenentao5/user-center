package com.cet.usercenter.common;

import lombok.Data;

import java.io.Serializable;

/**
 * @program: user-center
 * @description: 通用返回类
 * @author: 陈恩涛
 * @create: 2023-05-19 21:43
 **/

@Data
public class BaseResponse<T> implements Serializable {

    private static final long serialVersionUID = 6043832898300040965L;

    private int code;

    private T data;

    private String message;

    private String description;

    public BaseResponse(int code, T data, String message, String description) {
        this.code = code;
        this.data = data;
        this.message = message;
        this.description = description;
    }

    public BaseResponse(int code, T data) {
        this(code, data, "","");
    }

    public BaseResponse(int code, T data, String message) {
        this(code, data, message,"");
    }

    /**
     * 成功
     * @param data 返回结果
     * @return 通用返回结果
     * @param <T> 返回结果的数据类型
     */
    public static <T> BaseResponse<T> success(T data) {
        return new BaseResponse<>(0, data, "ok");
    }

    /**
     * 失败
     * @param errorCode 错误枚举
     * @return 通用返回结果
     * @param <T> 泛型
     */
    public static <T> BaseResponse<T> error(ErrorCode errorCode) {
        return new BaseResponse<>(errorCode.getCode(), null, errorCode.getMessage(), errorCode.getDescription());
    }

    public static <T> BaseResponse<T> error(int code, String message, String description) {
        return new BaseResponse<>(code, null, message, description);
    }

    public static <T> BaseResponse<T> error(ErrorCode errorCode, String message, String description) {
        return new BaseResponse<>(errorCode.getCode(), null, message, description);
    }


}
