package com.cet.usercenter.controller;

import com.cet.usercenter.common.BaseResponse;
import com.cet.usercenter.common.ErrorCode;
import com.cet.usercenter.exception.BusinessException;
import com.cet.usercenter.model.domain.User;
import com.cet.usercenter.model.request.UserLoginRequest;
import com.cet.usercenter.model.request.UserRegisterRequest;
import com.cet.usercenter.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

import static com.cet.usercenter.constant.UserConstant.ADMIN_ROLE;
import static com.cet.usercenter.constant.UserConstant.USER_LOGIN_STATE;

/**
 * @program: user-center
 * @description: 用户接口
 * @author: 陈恩涛
 * @create: 2023-05-18 22:11
 **/

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @PostMapping("/register")
    public BaseResponse<Long> userRegister(@RequestBody UserRegisterRequest registerRequest) {

        if (registerRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户注册请求为空！");
        }

        String userAccount = registerRequest.getUserAccount();
        String userPassword = registerRequest.getUserPassword();
        String checkPassword = registerRequest.getCheckPassword();
        String planetCode = registerRequest.getPlanetCode();

        //校验
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        long result = userService.userRegister(userAccount, userPassword, checkPassword, planetCode);
        return BaseResponse.success(result);

    }

    @PostMapping("/login")
    public BaseResponse<User> userLogin(@RequestBody UserLoginRequest loginRequest, HttpServletRequest request) {
        if (loginRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户登录请求为空！");
        }

        String userAccount = loginRequest.getUserAccount();
        String userPassword = loginRequest.getUserPassword();

        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        User user = userService.doLogin(userAccount, userPassword, request);
        return BaseResponse.success(user);
    }

    @PostMapping("/logout")
    public BaseResponse<Integer> userLogout(HttpServletRequest request) {
        if (request == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        int result = userService.userLogout(request);
        return BaseResponse.success(result);

    }

    @GetMapping("/current")
    public BaseResponse<User> getCurrentUser(HttpServletRequest request) {
        Object userObj = request.getSession().getAttribute(USER_LOGIN_STATE);
        User currentUser = (User) userObj;
        if (currentUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }

        Long userId = currentUser.getId();

        // todo 判断用户是否已经被封号
        User safetyUser = userService.getSafetyUser(userService.getById(userId));
        return BaseResponse.success(safetyUser);
    }

    @GetMapping("/search")
    public BaseResponse<List<User>> searchUsers(String username, HttpServletRequest request) {

        if (!isAdmin(request)) {
            throw new BusinessException(ErrorCode.NO_AUTH);
        }
        List<User> result = userService
            .lambdaQuery()
            .like(StringUtils.isNotBlank(username), User::getUsername, username)
            .list()
            .stream()
            .map(userService::getSafetyUser)
            .collect(Collectors.toList());
        return BaseResponse.success(result);
    }

    /**
     * 是否为管理员
     * @param request http请求
     * @return true 管理员 false 不是
     */
    private boolean isAdmin(HttpServletRequest request) {
        // 仅管理员可以查询
        Object userObj = request.getSession().getAttribute(USER_LOGIN_STATE);
        User user = (User) userObj;
        return user != null && user.getUserRole() == ADMIN_ROLE;
    }

    @PostMapping("/delete")
    public BaseResponse<Boolean> deleteUser(@RequestBody long id, HttpServletRequest request) {
        if (id <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        if (!isAdmin(request)) {
            throw new BusinessException(ErrorCode.NO_AUTH);
        }

        boolean result = userService.removeById(id);

        return BaseResponse.success(result);
    }


}
