package com.cet.usercenter.controller;

import com.cet.usercenter.common.BaseResponse;
import com.cet.usercenter.common.ErrorCode;
import com.cet.usercenter.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @program: user-center
 * @description: 全局异常处理
 * @author: 陈恩涛
 * @create: 2023-05-19 23:38
 **/

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(BusinessException.class)
    public BaseResponse<Void> businessExceptionHandler(BusinessException e) {
        log.error("businessException: " + e.getMessage(), e);
        return BaseResponse.error(e.getCode(), e.getMessage(), e.getDescription());
    }

    @ExceptionHandler(RuntimeException.class)
    public BaseResponse<Void> runtimeExceptionHandler(RuntimeException e) {
        log.error("runtimeException", e);
        return BaseResponse.error(ErrorCode.SYSTEM_ERROR, e.getMessage(), "");
    }

}
